[![Build Status](https://travis-ci.org/SAP/openui5-basic-template-app.svg?branch=master)](https://travis-ci.org/SAP/openui5-basic-template-app)
![OpenUI5 logo](http://openui5.org/images/OpenUI5_new_big_side.png)

# openui5-basic-template-app
OpenUI5 basic template app using the UI5 Build and Development Tooling. You can use it as a starting point to build custom OpenUI5 apps.

## More information
* [Live Demo](https://sap.github.io/openui5-basic-template-app)
* [Documentation](https://openui5.hana.ondemand.com/#/topic/7a4d93c0b0bb439b9d889ffc5b02eac9)
* [UI5 Tooling](https://github.com/SAP/ui5-tooling)
* [OpenUI5](https://github.com/SAP/openui5)

## Prerequisites
The **UI5 build and development tooling command line interface (UI5 CLI)** has to be installed.
For installation instructions please see [Installing the UI5 CLI](https://github.com/SAP/ui5-tooling#installing-the-ui5-cli).

## Setup
1. Clone the repository and navigate into it
    ```sh
    git clone https://github.com/SAP/openui5-basic-template-app.git
    cd openui5-basic-template-app
    ```
1. Install all dependencies
    ```sh
    npm install
    ```

1. Start a local server and run the application (http://localhost:8080/components/ShellComponent/index.html)
    ```sh
    ui5 serve -o /index.html
    ```

## Testing
* Run ESLint code validation
    ```sh
    npm run lint
    ```
* Start a local server and execute the tests automatically after every change
    ```sh
    npm run watch
    ```
* Run ESLint, start a local server and run the tests in CI mode
    ```sh
    npm test
    ```

For more build and development options please see: [UI5 Build and Development Tooling](https://github.com/SAP/ui5-tooling)

## Support
This repository is based on the [OpenUI5 template demo apps](https://openui5.hana.ondemand.com/#/demoapps) and updated regularly with our latest recommendations.
If you found a bug, please create an [OpenUI5 issue](https://github.com/sap/openui5/issues). Thank you!

# Folder Structure

```
|-- components
    |-- component 1
    |-- component 2
    |-- component 3
    |-- shell component 1
    |-- shell component 2
|-- shared
    |-- control.lib
    |-- libs
    |-- fragments
|-- resources
    |-- themes
```

# Features
Basieren auf der "Basic Template UI5 Applikation" befinden sich in der Shell Komponente vier Navigations Buttons.
- Nav to Categories
- Nav to Other
- Nav to Products
- Nav to Showcase

## Base Component
Stellt die `Component.js` und `*.controller.js` mit Basisfunktionalität zur Verfügung. Wird von den anderen Componenten und Controller in den anderen Componenten geerbt.

## Shell Component
Ist eine UI5-Basic-Template Applikation. Diese wurde um `routing` und `componentUsages` in der Manifest und das Einbinden der Resource Locator in der `index.html` Datei erweitert. Desweiteren stellt die App die bekannten vier Buttons zur Navigation zur Verfügung.

## Categories Component
Kopiert aus dem [UI5con Beispiel](https://flovogt.github.io/ui5con20-ui5-routing).
Funktionalität wie im Bsp.. Interaktion mit `ProductsComponent`.
Code in diesem Repository, wird über den im Projekt enthaltenen `ui5 serve` taks gestartet.

## Produts Component
Kopiert aus dem [UI5con Beispiel](https://flovogt.github.io/ui5con20-ui5-routing).
Funktionalität wie im Bsp.. Interaktion mit `CategoriesComponent`.
Code in diesem Repository, wird über den im Projekt enthaltenen `ui5 serve` taks gestartet.

## Others
keine Funktionalität -> TBD

## Showcase
Navigation zu der UI5 Sample Applikation "Shoppingcart". Diese Applikation wird weiterhin auf der SAP Platform betrieben, durch die ui5 middleware wird die Componente in dieses Projekt eingebunden.
