
sap.ui.define([
  'sap/ui/core/routing/Router',
  'sap/m/routing/Router',
  'sap/ui/core/routing/History',
  'sap/ui/core/routing/HashChanger',
  'sap/ui/base/ManagedObject'
], function (BaseRouter, Router, History, sapHashChanger, ManagedObject) {
  'use strict';
  return Router.extend('my.lib.sample.base.routing.Router', {


    /**
     * constructor
     */
    constructor: function () {
      Object.prototype.constructor.apply(this, arguments);

    },

  });
});
