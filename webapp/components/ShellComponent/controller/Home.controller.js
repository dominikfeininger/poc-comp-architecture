sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"my/lib/sample/base/BaseController",
	"../model/formatter"
], function (Controller, BaseController, formatter) {
	"use strict";

	return BaseController.extend("sap.ui.demo.basicTemplate.controller.App", {

		formatter: formatter,

		onInit: function () {

		},

		onNavigation: function (sKey) {
			this.getOwnerComponent().getRouter().navTo(sKey);
		}
	});
});
