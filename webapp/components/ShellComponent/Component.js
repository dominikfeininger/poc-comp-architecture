sap.ui.define([
	"sap/ui/core/UIComponent",
	"my/lib/sample/base/Component",
	"sap/ui/Device",
	"./model/models"
], function (UIComponent, Component, Device, models) {
	"use strict";

	return Component.extend("sap.ui.demo.basicTemplate.Component", {

		metadata: {
			manifest: "json"
		},
		// define the events which are fired from the reuse components
		//
		// this component registers handler to those events and navigates
		// to the other reuse components
		//
		// see the implementation in Component for processing the event
		// mapping
		eventMappings: {
			suppliersComponent: [{
				name: "toProduct",
				route: "productsRoute",
				componentTargetInfo: {
					productsTarget: {
						route: "detailRoute",
						parameters: {
							id: "productID"
						}
					}
				}
			}],
			productsComponent: [{
				name: "toSupplier",
				route: "suppliersRoute",
				componentTargetInfo: {
					suppliersTarget: {
						route: "detailRoute",
						parameters: {
							id: "supplierID"
						},
						componentTargetInfo: {
							productsTarget: {
								route: "listRoute",
								parameters: {
									basepath: "supplierKey"
								}
							}
						}
					}
				}
			}, {
				name: "toCategory",
				route: "categoriesRoute",
				componentTargetInfo: {
					categoriesTarget: {
						route: "detailRoute",
						parameters: {
							id: "categoryID"
						},
						componentTargetInfo: {
							productsTarget: {
								route: "listRoute",
								parameters: {
									basepath: "categoryKey"
								}
							}
						}
					}
				}
			}, {
				name: "toProduct",
				route: "productsRoute",
				componentTargetInfo: {
					productsTarget: {
						route: "detailRoute",
						parameters: {
							id: "productID"
						}
					}
				}
			}],
			categoriesComponent: [{
				name: "toProduct",
				route: "productsRoute",
				componentTargetInfo: {
					productsTarget: {
						route: "detailRoute",
						parameters: {
							id: "productID"
						}
					}
				}
			}]
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			Component.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			// create the views based on the url/hash
			this.getRouter().initialize();
		}
	});
});
